package chesig.controller;

import chesig.exceptions.NoCaddieFoundException;
import chesig.exceptions.ProductNotFoundException;
import chesig.models.Caddie;
import chesig.models.product.Product;
import chesig.service.CaddieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/caillou")
public class CaddieController {

    @Autowired
    private CaddieService caddieService;

    @GetMapping("/caddies/caddie/{id}")
    public ResponseEntity<Caddie> getCaddieWithId(@PathVariable(value="id") Long id) {

        Caddie c = null;
        try {
            c = caddieService.getCaddieWithId(id);
            return ResponseEntity.ok().body(c);
        } catch (NoCaddieFoundException e) {
            e.printStackTrace();
        }
        return ResponseEntity.ok().body(null);
    }

    @GetMapping("/caddies/mail")
    public List<Caddie> getCaddiesWithEmailAddress(@RequestParam String emailAddress) {
        try {
            return caddieService.getCaddiesWithEmailAddress(emailAddress);
        } catch (NoCaddieFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PostMapping("/caddies/post/")
    public Caddie createCaddie(@RequestParam String emailAddress) {
        return caddieService.createCaddie(new Caddie(emailAddress));
    }

    @PostMapping("/caddies/{id}/addProduct/{barcode}")
    public Product addProductToCaddie(@RequestParam @PathVariable(value="id") Long caddieId, @RequestParam @PathVariable(value="barcode")String barcode) {
        try {
            return caddieService.addProductToCaddie(caddieId, barcode);
        } catch (NoCaddieFoundException e) {
            e.printStackTrace();
        } catch (ProductNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @DeleteMapping("/caddies/caddie/{id}")
    public void deleteCaddie(@RequestParam @PathVariable(value="id") Long caddieId) {
        caddieService.removeCaddie(caddieId);
    }

    @DeleteMapping("caddie/{id}/deleteProduct/{barcode}")
    public void deleteProductFromCaddie(@RequestParam @PathVariable(value="id") Long caddieId, @RequestParam @PathVariable(value="barcode")String barcode){
        caddieService.removeProductFromCaddie(caddieId, barcode);
    }
}