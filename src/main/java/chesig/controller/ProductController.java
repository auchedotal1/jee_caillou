package chesig.controller;

import chesig.exceptions.ProductNotFoundException;
import chesig.models.product.NutritionInformation;
import chesig.models.product.Product;
import chesig.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/caillou")
public class ProductController {

    @Autowired
    private ProductService productService = new ProductService();

    @GetMapping("/products/{barcode}")
    public ResponseEntity<Product> getProduct(@PathVariable(value="barcode") String barcode) throws ProductNotFoundException {
        Product p = productService.findWithBarcode(barcode);
        return ResponseEntity.ok().body(p);
    }


    @GetMapping("/products/{barcode}/qualities")
    public ResponseEntity<NutritionInformation> getProductQualities(@PathVariable(value="barcode") String barcode) throws ProductNotFoundException {
        NutritionInformation info = productService.findProductQualitiesWithBarcode(barcode);

        return ResponseEntity.ok().body(info);
    }

}