package chesig.repository;

import chesig.models.Caddie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CaddieRepository extends JpaRepository<Caddie, Long> {
    List<Caddie> findByEmailAddress(String emailAddress);
}
