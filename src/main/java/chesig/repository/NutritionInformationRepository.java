package chesig.repository;

import chesig.models.product.NutritionInformation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NutritionInformationRepository extends JpaRepository<NutritionInformation, Long> {
}