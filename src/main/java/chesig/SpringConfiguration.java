package chesig;

import chesig.service.OpenFoodFactsHandler;
import chesig.service.OpenFoodFactsInterface;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class SpringConfiguration extends WebMvcConfigurationSupport {

    //Beans
    @Bean(name="OpenFoodFactHandler")
    public OpenFoodFactsInterface getOpenFoodFactsHandler() {
        return new OpenFoodFactsHandler();
    }

}
