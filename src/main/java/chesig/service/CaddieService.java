package chesig.service;

import chesig.exceptions.NoCaddieFoundException;
import chesig.exceptions.ProductNotFoundException;
import chesig.models.Caddie;
import chesig.models.product.NutritionInformation;
import chesig.models.product.Product;
import chesig.models.product.Quality;
import chesig.repository.CaddieRepository;
import chesig.repository.NutritionInformationRepository;
import chesig.repository.ProductRepository;
import chesig.repository.QualityRepository;
import com.sun.istack.internal.NotNull;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CaddieService {

    @Autowired
    private ProductService productService;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CaddieRepository caddieRepository;

    @Autowired
    private NutritionInformationRepository nutritionInformationRepository;

    @Autowired
    private QualityRepository qualityRepository;


    public Caddie createCaddie(@NotNull Caddie caddie) {
        return caddieRepository.save(caddie);
    }

    public Caddie getCaddieWithId(Long id) throws NoCaddieFoundException {
        Optional<Caddie> caddieTest = caddieRepository.findById(id);
        Caddie res = caddieTest.get();
        if (res == null) throw new NoCaddieFoundException(id);
        else {
            //initProductListInCaddie(res);
            return res;
        }
    }

    public List<Caddie> getCaddiesWithEmailAddress (String emailAddress) throws NoCaddieFoundException {
        List<Caddie> caddies = caddieRepository.findByEmailAddress(emailAddress);
        if (caddies == null) throw new NoCaddieFoundException(emailAddress);
        else {
            for (Caddie c : caddies){
                //initProductListInCaddie(c);
            }
            return caddies;
        }
    }

    public Product addProductToCaddie (Long idCaddie, String barcode) throws NoCaddieFoundException, ProductNotFoundException {

        Caddie c = getCaddieWithId(idCaddie);
        Product p = productService.findWithBarcode(barcode);
        c.addProduct(p);

        productRepository.save(p);
        caddieRepository.save(c); //est-ce nécessaire ?

        return p;
    }


    public void removeProductFromCaddie(Long idCaddie, String barcode) {
        try {
            Caddie c = getCaddieWithId(idCaddie);
            /*List<Product> products = productRepository.findAllByBarcodeAndCaddie(barcode, c);   //doesn't work, doesn't find anything

            System.out.println("ihih");//System.out.println(p.toString());

            if (products != null) {
                c.products.remove(products.get(0));
                productRepository.delete(products.get(0));
                caddieRepository.save(c); //nécessaire?
            }
             */

            try {
                Product p = productService.findWithBarcode(barcode);
                List<Product> products = productRepository.findAllByBarcode(barcode);

                for(Product temp : products) {
                    if(temp.caddie != null && temp.caddie.id == idCaddie) {
                        productRepository.delete(temp);
                        c.deleteProduct(temp);
                    }
                }
                //c.deleteProduct(p);
                caddieRepository.save(c);

            } catch (ProductNotFoundException e) {
                e.printStackTrace();
            }

        } catch (NoCaddieFoundException e) {
            e.printStackTrace();
        }
    }


    public void removeCaddie(Long idCaddie){
        try {
            Caddie c = getCaddieWithId(idCaddie);
            caddieRepository.delete(c); //check que ça delete bien les produits associés
        } catch (NoCaddieFoundException e) {
            e.printStackTrace();
        }
    }


}