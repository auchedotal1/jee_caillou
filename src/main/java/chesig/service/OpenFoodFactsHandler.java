package chesig.service;

import chesig.exceptions.ProductNotFoundException;
import chesig.models.product.Product;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class OpenFoodFactsHandler implements OpenFoodFactsInterface{

    //https://www.journaldev.com/7148/java-httpurlconnection-example-java-http-request-get-post
    //https://www.baeldung.com/java-http-request


    //https://dzone.com/articles/how-to-implement-get-and-post-request-through-simp
    //https://stackoverflow.com/questions/10500775/parse-json-from-httpurlconnection-object      OOOOKKK
    public Product getProduct(String barcode) throws IOException {
        URL url = new URL("https://fr.openfoodfacts.org/api/v0/produit/" + barcode);
        String readLine = null;
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.setUseCaches(false);
        connection.setAllowUserInteraction(false);
        connection.connect();

        String jsonReply;
        Product product = null;

        if(connection.getResponseCode()==201 || connection.getResponseCode()==200) {
            InputStream response = connection.getInputStream();
            jsonReply = convertStreamToString(response);

            //http://www.java67.com/2016/10/3-ways-to-convert-string-to-json-object-in-java.html
            //JSONParser parser = new JSONParser();
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(jsonReply);
                JSONObject jsonProduct = null;
                try {
                    jsonProduct = jsonObject.getJSONObject("product");
                    JSONObject nutriments = null;
                    try {
                        nutriments = jsonProduct.getJSONObject("nutriments");
                        product = new Product();
                        product.setEnergy_100g(Double.parseDouble(nutriments.getString("energy_100g")));
                        product.setSaturated_fat_100g(nutriments.getDouble("saturated-fat_100g"));
                        product.setSugars_100g(nutriments.getDouble("sugars_100g"));
                        product.setSalt_100g(nutriments.getDouble("salt_100g"));
                        product.setFiber_100g(Double.parseDouble(nutriments.getString("fiber_100g")));
                        product.setProteins_100g(nutriments.getDouble("proteins_100g"));
                        product.setNutritionalInfo();
                        product.barcode = barcode;
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //https://stackabuse.com/reading-and-writing-json-in-java/     to get only what we want in the json

        }

        return product;
    }



    private String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}
