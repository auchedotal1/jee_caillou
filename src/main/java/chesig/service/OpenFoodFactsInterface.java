package chesig.service;

import chesig.exceptions.ProductNotFoundException;
import chesig.models.product.Product;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public interface OpenFoodFactsInterface {

    Product getProduct(String barcode) throws IOException;
}
