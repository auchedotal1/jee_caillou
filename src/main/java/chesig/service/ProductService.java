package chesig.service;

import chesig.exceptions.ProductNotFoundException;
import chesig.models.product.NutritionInformation;
import chesig.models.product.Product;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;

@Service
public class ProductService {

    @Resource(name="OpenFoodFactHandler")
    private OpenFoodFactsInterface openFoodFacts;

    public Product findWithBarcode(String barcode) throws ProductNotFoundException {
        try {
            Product p = openFoodFacts.getProduct(barcode);
            if(p == null) throw new ProductNotFoundException(barcode);
            return p;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public NutritionInformation findProductQualitiesWithBarcode(String barcode) throws ProductNotFoundException {
        try {
            Product p = openFoodFacts.getProduct(barcode);
            if(p == null) throw new ProductNotFoundException(barcode);
            return p.nutritionInformation;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
