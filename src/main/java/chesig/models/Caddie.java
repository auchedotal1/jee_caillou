package chesig.models;

import chesig.models.product.Product;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.*;

@Entity
@Table(name = "caddies")
public class Caddie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public Long id;

    @Column(name = "email")
    public String emailAddress;

    @OneToMany(fetch = FetchType.EAGER, cascade={PERSIST, MERGE, REMOVE, REFRESH, DETACH})
    public List<Product> products;

    public Caddie() {
        emailAddress = "aaa@bbb.com";
        products = new ArrayList<>();
    }

    public Caddie(String email) {
        this.emailAddress = email;
        products = new ArrayList<>();
    }

    @Override
    public String toString(){
        return "Caddie n°" + Long.toString(id) + ", email : " + emailAddress + "\n" + products.toString();
    }

    public void addProduct(Product p) {
        products.add(p);
    }

    public void deleteProduct(Product product) {
        for(Product p : products) {
            if (p.barcode == product.barcode) products.remove(p);
        }
    }
}
