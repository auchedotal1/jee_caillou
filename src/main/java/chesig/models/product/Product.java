package chesig.models.product;

import chesig.models.Caddie;
import chesig.models.product.NutritionOperation;

import javax.persistence.*;
import java.util.Map;

import static javax.persistence.CascadeType.*;

@Entity
@Table(name = "products")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public long id;

    @ManyToOne(cascade={PERSIST, MERGE, REMOVE, REFRESH, DETACH})
    //@JoinColumn(name = "caddie_id")
    public Caddie caddie;

    @Column (name = "barcode")
    public String barcode;

    @Column(name = "energy")
    public double energy_100g;

    @Column(name = "fat")
    public double saturated_fat_100g; //saturated-fat_100g;

    @Column(name = "sugar")
    public double sugars_100g;

    @Column(name = "salt")
    public double salt_100g;

    @Column(name = "fiber")
    public double fiber_100g;

    @Column(name = "protein")
    public double proteins_100g;

    @Column(name = "nutritionalScore")
    public int nutritionalScore = 0;

    @Column(name = "positiveScore")
    public int positiveScore = 0;

    @Column(name = "negativeScore")
    public int negativeScore = 0;

    @OneToOne(cascade={PERSIST, MERGE, REMOVE, REFRESH, DETACH})
    public NutritionInformation nutritionInformation;

    public Product () { }

    //operation
    public void setNutritionalInfo () {
        NutritionOperation.clear();
        positiveScore = NutritionOperation.getPositiveScore(fiber_100g, proteins_100g);
        negativeScore = NutritionOperation.getNegativeScore(energy_100g, saturated_fat_100g, sugars_100g, salt_100g);
        nutritionalScore = negativeScore - positiveScore;
        int temp1 = positiveScore;
        int temp2 = negativeScore;
        int temp3 = nutritionalScore;
        //qualities = NutritionOperation.getQualities();
        //drawbacks = NutritionOperation.getDrawbacks();

        nutritionInformation = new NutritionInformation(temp3, temp1, temp2, NutritionOperation.getQualities(), NutritionOperation.getDrawbacks());
    }


    // GETTER
    public double getEnergy_100g() {
        return energy_100g;
    }
    public double getSaturated_fat_100g() {
        return saturated_fat_100g;
    }
    public double getSugars_100g() {
        return sugars_100g;
    }
    public double getSalt_100g() {
        return salt_100g;
    }
    public double getFiber_100g() {
        return fiber_100g;
    }
    public double getProteins_100g() {
        return proteins_100g;
    }
    public int getNutritionalScore() {return nutritionalScore;}
    public int getPositiveScore() {return positiveScore;}
    public int getNegativeScore() {return negativeScore;}
//    public Map<String, Integer> getQualities () {return qualities;}
//    public Map<String, Integer> getDrawbacks () {return drawbacks;}


    // SETTER
    public void setEnergy_100g(double energy_100g) {
        this.energy_100g = energy_100g;
    }
    public void setSaturated_fat_100g(double saturated_fat_100g) {
        this.saturated_fat_100g = saturated_fat_100g;
    }
    public void setSugars_100g(double sugars_100g) {
        this.sugars_100g = sugars_100g;
    }
    public void setSalt_100g(double salt_100g) {
        this.salt_100g = salt_100g;
    }
    public void setFiber_100g(double fiber_100g) {
        this.fiber_100g = fiber_100g;
    }
    public void setProteins_100g(double proteins_100g) {
        this.proteins_100g = proteins_100g;
    }
    public void setNutrionalScore(int score) {nutritionalScore=score;}
    public void setPositiveScore(int score) {positiveScore=score;}
    public void setNegativeScore(int score) {negativeScore=score;}

    //
    @Override
    public String toString() {
        return "Energy/100g : " + Double.toString(energy_100g) + "\nFat/100g : " + Double.toString(saturated_fat_100g) + "\nSugar/100g : " + Double.toString(sugars_100g)
                + "\nSalt/100g : " +Double.toString(salt_100g) + "\nFiber/100g : " +Double.toString(fiber_100g) +"\nProtein/100g : " +Double.toString(proteins_100g);
    }
}
