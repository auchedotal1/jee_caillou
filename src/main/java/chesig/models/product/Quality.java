package chesig.models.product;

import javax.persistence.*;

import static javax.persistence.CascadeType.*;

@Entity
@Table(name = "qualities")
public class Quality {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public long id;

    @ManyToOne(cascade={PERSIST, MERGE, REMOVE, REFRESH, DETACH})
    private NutritionInformation nutritionInformation;

    @Column(name = "name")
    public String name;

    @Column(name = "score")
    public int score;

    public Quality(){}

    public Quality(String name, int score) {
        this.name = name;
        this.score = score;
    }

    @Override
    public String toString() {
        return name + ", score : " + Integer.toString(score);
    }
}
