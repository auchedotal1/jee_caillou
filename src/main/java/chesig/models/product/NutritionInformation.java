package chesig.models.product;

import javax.persistence.*;
import java.util.List;
import java.util.Map;

import static javax.persistence.CascadeType.*;

@Entity
@Table(name = "NutritionInformation")
public class NutritionInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    public long id;

    @OneToMany(cascade={PERSIST, MERGE, REMOVE, REFRESH, DETACH})
    public List<Quality> qualities;

    @OneToMany(cascade={PERSIST, MERGE, REMOVE, REFRESH, DETACH})
    public List<Quality> drawbacks;

    @Column(name = "nutritionalScore")
    public int nutritionalScore = 0;

    @Column(name = "positiveScore")
    public int positiveScore = 0;

    @Column(name = "negativeScore")
    public int negativeScore = 0;

    public NutritionInformation(){}

    public NutritionInformation(int nutritionalScore, int positiveScore, int negativeScore, List<Quality> qualities, List<Quality> drawbacks) {
        this.nutritionalScore = nutritionalScore;
        this.positiveScore = positiveScore;
        this.negativeScore = negativeScore;
        this.qualities = qualities;
        this.drawbacks = drawbacks;
    }

    public NutritionInformation(Product p){
        //qualities = p.getQualities();
        //drawbacks = p.getDrawbacks();
        nutritionalScore = p.nutritionalScore;
        positiveScore = p.positiveScore;
        negativeScore = p.negativeScore;
    }

    @Override
    public String toString(){
        return "Nutrional Information : \n" +
                "Nutritional Score : " + Integer.toString(nutritionalScore) + "\nPositive Score : " + Integer.toString(positiveScore) + "\nNegative Score : " + Integer.toString(negativeScore)
                + "\nQualities : " +qualities.toString() + "\nDrawbacks : " + drawbacks.toString();
    }
}
