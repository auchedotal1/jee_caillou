package chesig.models.product;

import com.sun.deploy.util.ArrayUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NutritionOperation {
    private static List<Quality> qualities = new ArrayList<>();
    private static  List<Quality> drawbacks = new ArrayList<>();
    private static int nutritionalScore;
    private static int negativeScore;
    private static int positiveScore;

    public static void clear() {
        qualities.clear();
        drawbacks.clear();
    }

    public static List<Quality> getQualities() {return qualities;}
    public static List<Quality> getDrawbacks() {return drawbacks;}

    public static int getNutritionalScore() {
        nutritionalScore = negativeScore - positiveScore;
        return nutritionalScore;
    }

    public static int getNegativeScore(double energy, double fat, double sugar, double salt)
    {
        return calcEnergyScore(energy) + calcFatScore(fat) + calcSugarScore(sugar) + calcSaltScore(salt);
    }

    public static int getPositiveScore(double fiber, double protein)
    {
        return calcFiberScore(fiber) + calcProteinScore(protein);
    }

    private static int calcEnergyScore(double energy) {
        int score = 0;
        if (energy > 3350) score = 10;
        else if (energy > 3015) score = 9;
        else if (energy > 2680) score = 8;
        else if (energy > 2345) score = 7;
        else if (energy > 2010) score = 6;
        else if (energy > 1675) score = 5;
        else if (energy > 1340) score = 4;
        else if (energy > 1005) score = 3;
        else if (energy > 670) score = 2;
        else if (energy > 335) score = 1;

        if (score < 4) qualities.add(new Quality("ENERGY", score));
        if (score > 6) drawbacks.add(new Quality("ENERGY", score));

        return score;
    }

    private static int calcFatScore(double fat) {
        int score = 0;
        if (fat > 10) score = 10;
        else if (fat > 9) score = 9;
        else if (fat > 8) score = 8;
        else if (fat > 7) score = 7;
        else if (fat > 6) score = 6;
        else if (fat > 5) score = 5;
        else if (fat > 4) score = 4;
        else if (fat > 3) score = 3;
        else if (fat > 2) score = 2;
        else if (fat > 1) score = 1;

        if (score < 4) qualities.add(new Quality("FAT", score));
        if (score > 6) drawbacks.add(new Quality("FAT", score));

        return score;
    }

    private static int calcSugarScore(double sugar) {
        int score = 0;
        if (sugar > 45) score = 10;
        else if (sugar > 40) score = 9;
        else if (sugar > 36) score = 8;
        else if (sugar > 31) score = 7;
        else if (sugar > 27) score = 6;
        else if (sugar > 22.5) score = 5;
        else if (sugar > 18) score = 4;
        else if (sugar > 13.5) score = 3;
        else if (sugar > 9) score = 2;
        else if (sugar > 4.5) score = 1;

        if (score < 4) qualities.add(new Quality("SUGAR", score));
        if (score > 6) drawbacks.add(new Quality("SUGAR", score));

        return score;
    }

    private static int calcSaltScore(double salt) {
        int score = 0;
        if (salt > 900) score = 10;
        else if (salt > 810) score = 9;
        else if (salt > 720) score = 8;
        else if (salt > 630) score = 7;
        else if (salt > 540) score = 6;
        else if (salt > 450) score = 5;
        else if (salt > 360) score = 4;
        else if (salt > 270) score = 3;
        else if (salt > 180) score = 2;
        else if (salt > 90) score = 1;

        if (score < 4) qualities.add(new Quality("SALT", score));
        if (score > 6) drawbacks.add(new Quality("SALT", score));

        return score;
    }

    private static int calcFiberScore(double fiber) {
        int score = 0;
        if (fiber > 4.7) score = 5;
        else if (fiber > 3.7) score = 4;
        else if (fiber > 2.8) score = 3;
        else if (fiber > 1.9) score = 2;
        else if (fiber > 0.9) score = 1;

        if (score > 1) qualities.add(new Quality("FIBER", score));
        if (score <= 0) drawbacks.add(new Quality("FIBER", score));

        return score;
    }

    private static int calcProteinScore(double protein) {
        int score = 0;
        if (protein > 8) score = 5;
        else if (protein > 6.4) score = 4;
        else if (protein > 4.8) score = 3;
        else if (protein > 3.2) score = 2;
        else if (protein > 1.6) score = 1;

        if (score > 1) qualities.add(new Quality("PROTEIN", score));
        if (score <= 0) drawbacks.add(new Quality("PROTEIN", score));

        return score;
    }
}
