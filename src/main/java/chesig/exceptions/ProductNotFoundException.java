package chesig.exceptions;

public class ProductNotFoundException extends Exception{
    public ProductNotFoundException(String barCode) {super("No product found with the following barcode: " + barCode);}
}
