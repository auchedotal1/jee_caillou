package chesig.exceptions;

public class NoCaddieFoundException extends Exception{
    public NoCaddieFoundException(String emailAddress) {super("No caddie found with the following email address: " + emailAddress);}
    public NoCaddieFoundException(Long id) {super("No caddie found with the following email address: " + Long.toString(id));}
}
