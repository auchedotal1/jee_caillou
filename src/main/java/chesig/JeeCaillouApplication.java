package chesig;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JeeCaillouApplication {


	public static void main(String[] args) {

		SpringApplication.run(JeeCaillouApplication.class, args);
	}

}
