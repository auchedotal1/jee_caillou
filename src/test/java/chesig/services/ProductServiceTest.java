package chesig.services;

import chesig.exceptions.ProductNotFoundException;
import chesig.models.product.Product;
import chesig.service.ProductService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTest {

    @Autowired
    private ProductService productService;

    @Test
    public void testOpenFoodFactsApiGet () throws ProductNotFoundException {

        Product p = productService.findWithBarcode("737628064502");

//        for(String s : p.getQualities().keySet()){
//            System.out.println(s);
//        }
//        for(String s : p.getDrawbacks().keySet()){
//            System.out.println(s);
//        }

        assertEquals("", p.getEnergy_100g(), 1660.0, 0.0001);
        assertEquals("", p.getSaturated_fat_100g(), 1.28, 0.0001);
        assertEquals("", p.getSugars_100g(), 12.8, 0.0001);
        assertEquals("", p.getSalt_100g(), 2.05, 0.0001);
        assertEquals("", p.getFiber_100g(), 0.0, 0.0001);
        assertEquals("", p.getProteins_100g(), 8.97, 0.0001);

        assertEquals(p.getNegativeScore(), 7);
        assertEquals(p.getPositiveScore(), 5);
        assertEquals(p.getNutritionalScore(), 2);

    }
}
