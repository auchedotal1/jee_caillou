package chesig.services;

import chesig.exceptions.NoCaddieFoundException;
import chesig.exceptions.ProductNotFoundException;
import chesig.models.Caddie;
import chesig.models.product.Product;
import chesig.service.CaddieService;
import chesig.service.ProductService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CaddieServiceTest {

    @Autowired
    private CaddieService caddieService;

    @Autowired
    private ProductService productService;

    @Test
    public void CreateCaddieTest () {
        Caddie c = new Caddie("email@ok.com");
        Caddie cSave = caddieService.createCaddie(c);

        Assert.assertEquals(c.emailAddress, cSave.emailAddress);
        Assert.assertEquals(c, cSave);
    }

    //Tout s'enregistre bien y compris NutritionInformation et Quality
    @Test
    public void CreateCaddieWithProductsTest(){
        Caddie c = new Caddie("email2@ok.com");
        Product p = null;
        try {
            p = productService.findWithBarcode("737628064502");
            c.products.add(p);

            Caddie cSave = caddieService.createCaddie(c);

            System.out.println(c.toString());
            System.out.println(c.products.toString());
            System.out.println(cSave.toString());
            System.out.println(cSave.products.toString());

            Assert.assertEquals(c.emailAddress, cSave.emailAddress);
            Assert.assertEquals(c, cSave);
        } catch (ProductNotFoundException e) {
            e.printStackTrace();
        }

    }


    @Test
    public void FindCaddieWithEmailAddress(){
        String emailAddress = "withEmail@ok.com";
        caddieService.createCaddie(new Caddie(emailAddress));
        caddieService.createCaddie(new Caddie(emailAddress));

        try {
            List<Caddie> caddies = caddieService.getCaddiesWithEmailAddress(emailAddress);

            Assert.assertEquals(caddies.size(), 2);

        } catch (NoCaddieFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void FindCaddieWithId(){
        caddieService.createCaddie(new Caddie("withid@ok.com"));
        try {
            List<Caddie> caddies = caddieService.getCaddiesWithEmailAddress("withid@ok.com");

            Assert.assertEquals(caddies.size(), 1);

            Caddie c = caddieService.getCaddieWithId(caddies.get(0).id);

            Assert.assertEquals(c.emailAddress,"withid@ok.com" );
            Assert.assertEquals(c.products.size(), 0);
        } catch (NoCaddieFoundException e) {
            e.printStackTrace();
        }
    }


    @Test
    public void AddProductToCaddie() throws NoCaddieFoundException, ProductNotFoundException {
        Caddie temp = caddieService.createCaddie(new Caddie("addproduct@ok.com"));
        List<Caddie> caddies = caddieService.getCaddiesWithEmailAddress("addproduct@ok.com");
        Product p = productService.findWithBarcode("737628064502");

        System.out.println("test");
        Caddie c = caddies.get(0);
        System.out.println(c.toString());
        //Assert.assertNull(c.products);
        //System.out.println(c.products.toString());


        Assert.assertEquals(c.products.size(), 0);
        caddieService.addProductToCaddie(caddies.get(0).id, "737628064502");
        caddies = caddieService.getCaddiesWithEmailAddress("addproduct@ok.com");
        Assert.assertEquals(caddies.get(0).products.size(), 1);
    }


    @Test
    public void DeleteProductFromCaddie() throws NoCaddieFoundException, ProductNotFoundException {
        System.out.println("delete product test");

        Caddie temp = caddieService.createCaddie(new Caddie("deleteproduct@ok.com"));
        List<Caddie> caddies = caddieService.getCaddiesWithEmailAddress("deleteproduct@ok.com");
        Product p = productService.findWithBarcode("737628064502");

        Caddie c = caddies.get(0);
        Long id = c.id;
        System.out.println("\n\ndebut");
        System.out.println(caddies.get(0));

        //Assert.assertEquals(c.products.size(), 0);
        caddieService.addProductToCaddie(caddies.get(0).id, "737628064502");
        caddies = caddieService.getCaddiesWithEmailAddress("deleteproduct@ok.com");
        System.out.println("\nmileu");
        System.out.println(caddies.get(0));
        //Assert.assertEquals(caddies.get(0).products.size(), 1);

        caddieService.removeProductFromCaddie(id, "737628064502");
        caddies = caddieService.getCaddiesWithEmailAddress("deleteproduct@ok.com");

        System.out.println("\nfin");
        System.out.println(caddies.get(0));
        System.out.println("\nproduit");
        System.out.println(caddies.get(0).products.get(0).caddie);

        Assert.assertEquals(caddies.get(0).products.size(), 0);
    }

    @Test
    public void DeleteCaddieTest() throws NoCaddieFoundException {
        Caddie temp = caddieService.createCaddie(new Caddie("delete@ok.com"));

        List<Caddie> list = caddieService.getCaddiesWithEmailAddress("delete@ok.com");
        Assert.assertEquals(list.size(), 1);

        caddieService.removeCaddie(temp.id);
        List<Caddie> list2 = caddieService.getCaddiesWithEmailAddress("delete@ok.com");
        Assert.assertEquals(list.size(), 0);
    }



}
