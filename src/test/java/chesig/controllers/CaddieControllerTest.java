package chesig.controllers;

import chesig.models.Caddie;
import chesig.models.product.Product;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CaddieControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @LocalServerPort
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    @Test
    public void createCaddie(){
        ResponseEntity<Caddie> postResponse  = restTemplate.postForEntity(getRootUrl()+"/caillou/caddies/post/", new Caddie("test@test.com"), Caddie.class);
        Assert.assertNotNull(postResponse);
        Assert.assertNotNull(postResponse.getBody());
    }


    @Test
    public void getCaddieWithId(){
        ResponseEntity<Caddie> postResponse  = restTemplate.postForEntity(getRootUrl()+"/caillou/caddies/post/", new Caddie("test@test.com"), Caddie.class);
        Caddie c = restTemplate.getForObject(getRootUrl()+"/caillou/caddies/1", Caddie.class);
        Assert.assertNotNull(c);
    }

    @Test
    public void getCaddiesWithEmailAddress(){
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(getRootUrl()+"/caillou/caddies/" + "?userMail=" + "aaabbb@ccc.com", HttpMethod.GET, entity, String.class);

        System.out.println(response.getBody());
        Assert.assertNotNull(response.getBody());
    }

    @Test
    public void addProductToCaddie(){
        /*Caddie c = restTemplate.getForObject(getRootUrl()+"/caillou//caddies/{id}/addProduct/{barcode}t", Caddie.class);
        System.out.println(c.toString());
        Assert.assertNotNull(c);*/
    }
}
