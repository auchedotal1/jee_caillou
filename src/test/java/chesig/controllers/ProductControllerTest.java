package chesig.controllers;

import chesig.models.product.NutritionInformation;
import chesig.models.product.Product;
import chesig.service.ProductService;
import javafx.application.Application;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ProductControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;


    @LocalServerPort
    private int port;

    private String getRootUrl() {
        return "http://localhost:" + port;
    }

    //test reception d'un produit via l'api
    @Test
    public void getProductWithBarcode(){
        Product p = restTemplate.getForObject(getRootUrl()+"/caillou/products/737628064502", Product.class);
        System.out.println(p.toString());
        Assert.assertNotNull(p);
    }

    //test reception des qualités d'un produit via l'api
    @Test
    public void getProductNutritionInfoWithBarcode(){
        NutritionInformation ni = restTemplate.getForObject(getRootUrl()+"/caillou/products/737628064502/qualities", NutritionInformation.class);
        System.out.println(ni.toString());
        Assert.assertNotNull(ni);
    }
}
